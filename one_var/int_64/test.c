#include <stdio.h>
#include <stdint.h>
int main() {
    int64_t a;
    scanf("%ld", &a);

    if (a > 5 && a < 11) {    
        printf("Good Job!\n");
    } else {
        printf("Bad Job!\n");
    }

    return 0;
}