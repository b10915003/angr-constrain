import angr
import sys
import claripy
def Go():
    path_to_binary = "./var_1_int64"
    project = angr.Project(path_to_binary, auto_load_libs=False)
    
    base_addr = project.loader.main_object.min_addr
    print("base_addr: ", hex(base_addr))

    start_address = 0x011bc + base_addr

    initial_state = project.factory.blank_state(addr=start_address)

    initial_state.regs.rbp = initial_state.regs.rsp
 
    symbol_size_in_bit = 64
    symbol_a = claripy.BVS('a', symbol_size_in_bit)

    padding_length_in_bytes = 0x10 - 8
    initial_state.regs.rsp -= padding_length_in_bytes
    
    initial_state.stack_push(symbol_a)  

    simulation = project.factory.simgr(initial_state)

    def is_successful(state):
        stdout_output = state.posix.dumps(1)
        if b"Good Job!\n" in stdout_output:
            return True
        else: 
            return False

    def should_abort(state):
        stdout_output = state.posix.dumps(1)
        if b"Bad Job!\n" in  stdout_output:
            return True
        else: 
            return False
    
    simulation.explore(find=is_successful, avoid=should_abort)

    
    file = open('ans.txt', 'w', encoding='utf-8')

    if simulation.found:
        for i in simulation.found:
            solution_state = i
            solution0 = solution_state.solver.eval_upto(symbol_a, 10)
            print("[+] Success! Solution is: {0}".format(solution0))
            for ans in solution0:
                file.write("{}\n".format(ans))
    else:
        raise Exception('Could not find the solution')
    file.close()
if __name__ == "__main__":
    Go()