import angr
import sys
import claripy
import angrcli.plugins.ContextView

def is_successful(state):
        stdout_output = state.posix.dumps(1)
        if b"Good Job!\n" in stdout_output:
            return True
        else: 
            return False

def should_abort(state):
    stdout_output = state.posix.dumps(1)
    if b"Bad Job!\n" in  stdout_output:
        return True
    else: 
        return False
    
def Go():
    path_to_binary = "./var_1_int"
    project = angr.Project(path_to_binary, auto_load_libs=False)
    
    base_addr = project.loader.main_object.min_addr
    print("base_addr: ", hex(base_addr))

    start_address = 0x011BC + base_addr

    initial_state = project.factory.blank_state(addr=start_address)

    initial_state.regs.rbp = initial_state.regs.rsp
 
    symbol_size_in_bit = 32

    symbol_a = claripy.BVS('a', symbol_size_in_bit)
    symbol_ignore = claripy.BVS('dont_care', symbol_size_in_bit)
    symbol_ab = claripy.Concat(symbol_ignore, symbol_a)

    padding_length_in_bytes = 0x0c - 8
    initial_state.regs.rsp -= padding_length_in_bytes
    
    initial_state.stack_push(symbol_ab) 

    simulation = project.factory.simgr(initial_state)

    
    
    simulation.explore(find=is_successful)

    file = open('ans.txt', 'w', encoding='utf-8')

    if simulation.found:
        for i in simulation.found:
            solution_state = i
            solution0 = solution_state.solver.eval_upto(symbol_ab, 10)
            list_a, list_b = [], []
            for ans in solution0:
                symbol_ans = claripy.BVV(ans, 64)
                symbol_ignore = solution_state.solver.eval(claripy.Extract(63, 32, symbol_ans))
                list_b.append(symbol_ignore)
                symbol_a = solution_state.solver.eval(claripy.Extract(31, 0, symbol_ans))
                list_a.append(symbol_a)
                file.write("{0},{1}\n".format(symbol_a, symbol_ignore))
            print("symbol_a: ", list_a)
            print("symbol_b: ", list_b)
    else:
        raise Exception('Could not find the solution')
    file.close()

def Go2():
    path_to_binary = "./var_1_int"
    project = angr.Project(path_to_binary, auto_load_libs=False)
    
    base_addr = project.loader.main_object.min_addr
    print("base_addr: ", hex(base_addr))

    start_address = 0x011BC + base_addr

    initial_state = project.factory.blank_state(addr=start_address)

    initial_state.regs.rbp = initial_state.regs.rsp
 
    symbol_size_in_bit = 32

    symbol_a = claripy.BVS('a', symbol_size_in_bit)
    symbol_ignore = claripy.BVV(0, symbol_size_in_bit)
    symbol_ab = claripy.Concat(symbol_ignore, symbol_a)

    
    padding_length_in_bytes = 0x0c - 8
    initial_state.regs.rsp -= padding_length_in_bytes
    
    initial_state.stack_push(symbol_ab)
    
    initial_state.context_view.pprint()

    simulation = project.factory.simgr(initial_state)

    
    
    simulation.explore(find=is_successful)

    file = open('ans.txt', 'w', encoding='utf-8')

    if simulation.found:
        for i in simulation.found:
            solution_state = i
            solution0 = solution_state.solver.eval_upto(symbol_ab, 10)
            list_a, list_b = [], []
            for ans in solution0:
                symbol_ans = claripy.BVV(ans, 64)
                symbol_ignore = solution_state.solver.eval(claripy.Extract(63, 32, symbol_ans))
                list_b.append(symbol_ignore)
                symbol_a = solution_state.solver.eval(claripy.Extract(31, 0, symbol_ans))
                list_a.append(symbol_a)
                file.write("{0}\n".format(symbol_a))
            print("symbol_a: ", list_a)
            print("symbol_b: ", list_b)
    else:
        raise Exception('Could not find the solution')
    file.close()

if __name__ == "__main__":
    Go2()
