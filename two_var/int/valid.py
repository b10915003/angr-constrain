import subprocess

binary_file = "./var_2_int"
def test_binary(binary_path, input_data):
    # 創建一個子進程來執行二進制文件
    process = subprocess.Popen([binary_path], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # 向 stdin 寫入測試數據
    process.stdin.write(input_data.encode())
    process.stdin.close()

    # 等待程序執行結束
    process.wait()

    # 讀取 stdout 和 stderr
    stdout = process.stdout.read().decode()
    stderr = process.stderr.read().decode()

    # 返回 stdout 和 stderr
    return stdout, stderr

if __name__ == "__main__":
    file = open('ans.txt', 'r', encoding='utf-8')
    for ans in file:
        # sperate the ','
        ans = ans.split(',')
        stdin_str = ""
        for i in ans:
            stdin_str = stdin_str + i + " "
        stdin_str = stdin_str[:-1]
        stdout, stderr = test_binary(binary_file,str(stdin_str))
        print("input = {} ans = {}".format(ans, stdout))
    # print("stderr: {}".format(stdout))