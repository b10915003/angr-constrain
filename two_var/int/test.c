#include <stdio.h>
#include <stdint.h>
int main() {
    int a, b;
    scanf("%d %d", &a, &b);

    if (a > 5 && a < 11) {
        if (b > 0 && b < 6) {
            printf("Good Job!\n");
        }
        else {
            printf("Bad Job!\n");
        }
    } else {
        printf("Bad Job!\n");
    }

    return 0;
}