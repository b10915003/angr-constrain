import angr
import sys
import claripy

success_output = [b"case1\n", b"case2\n", b"case3\n", b"case4\n", b"case5\n"]

def Go():
    path_to_binary = "./var_4_int64" 
    project = angr.Project(path_to_binary, auto_load_libs=False)

    base_addr = project.loader.main_object.min_addr
    print("base_addr: ", hex(base_addr))

    start_address = 0x011CB + base_addr

    initial_state = project.factory.blank_state(addr=start_address)

    initial_state.regs.rbp = initial_state.regs.rsp
 
    symbol_size_in_bit = 64
    symbol_a = claripy.BVS('a', symbol_size_in_bit)
    symbol_b = claripy.BVS('b', symbol_size_in_bit)
    symbol_c = claripy.BVS('c', symbol_size_in_bit)
    symbol_d = claripy.BVS('d', symbol_size_in_bit)

    padding_length_in_bytes = 0x0C - 4
    initial_state.regs.rsp -= padding_length_in_bytes
    
    initial_state.stack_push(symbol_d) 
    initial_state.stack_push(symbol_c)
    initial_state.stack_push(symbol_b)
    initial_state.stack_push(symbol_a)  
    
    simulation = project.factory.simgr(initial_state)
    
    def is_successful(state):
        stdout_output = state.posix.dumps(1)
        if b"case1\n" in stdout_output:
            return True
        else: 
            return False

    def should_abort(state):
        stdout_output = state.posix.dumps(1)
        if b'Try again.\n' in  stdout_output:
            return True
        else: 
            return False

    simulation.explore(find=is_successful)

    file = open('ans.txt', 'w', encoding='utf-8')

    if simulation.found:
        for i in simulation.found:
            solution_state = i
            solution0 = solution_state.solver.eval_upto(symbol_a, 10)
            solution1 = solution_state.solver.eval_upto(symbol_b, 10)
            solution2 = solution_state.solver.eval_upto(symbol_c, 10)
            solution3 = solution_state.solver.eval_upto(symbol_d, 10)

            for i in range(0,len(solution0)):
                file.write("{0},{1},{2},{3}\n".format(solution0[i], solution1[i], solution2[i], solution3[i]))
                
            print("symbol_a: ", solution0)
            print("symbol_b: ", solution1)
            print("symbol_a: ", solution2)
            print("symbol_b: ", solution3)
    else:
        raise Exception('Could not find the solution')
    file.close()
if __name__ == "__main__":
    Go()    