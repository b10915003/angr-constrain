import angr
import sys
import claripy

success_output = [b"case1\n", b"case2\n", b"case3\n", b"case4\n", b"case5\n"]

def Go():
    path_to_binary = "./var_4_int" 
    project = angr.Project(path_to_binary, auto_load_libs=False)

    base_addr = project.loader.main_object.min_addr
    print("base_addr: ", hex(base_addr))

    start_address = 0x011CB + base_addr

    initial_state = project.factory.blank_state(addr=start_address)

    initial_state.regs.rbp = initial_state.regs.rsp
 
    symbol_size_in_bit = 32
    symbol_a = claripy.BVS('a', symbol_size_in_bit)
    symbol_b = claripy.BVS('b', symbol_size_in_bit)
    symbol_c = claripy.BVS('c', symbol_size_in_bit)
    symbol_d = claripy.BVS('d', symbol_size_in_bit)

    symbol_ab = claripy.Concat(symbol_a, symbol_b)
    symbol_cd = claripy.Concat(symbol_c, symbol_d)

    padding_length_in_bytes = 0x0C - 4
    initial_state.regs.rsp -= padding_length_in_bytes
    
    initial_state.stack_push(symbol_cd) 
    initial_state.stack_push(symbol_ab)  
    


    simulation = project.factory.simgr(initial_state)
    
    def is_successful(state):
        stdout_output = state.posix.dumps(1)
        if b"case1\n" in stdout_output:
            return True
        else: 
            return False

    def should_abort(state):
        stdout_output = state.posix.dumps(1)
        if b'Try again.\n' in  stdout_output:
            return True
        else: 
            return False

    simulation.explore(find=is_successful)

    file = open('ans.txt', 'w', encoding='utf-8')

    if simulation.found:
        for i in simulation.found:
            solution_state = i
            solution0 = solution_state.solver.eval_upto(symbol_ab, 10)
            solution1 = solution_state.solver.eval_upto(symbol_cd, 10)
            list_a, list_b, list_c, list_d = [], [], [], []

            for ans in solution0:
                symbol_ans = claripy.BVV(ans, 64)
                symbol_a = solution_state.solver.eval(claripy.Extract(31, 0, symbol_ans))
                symbol_b = solution_state.solver.eval(claripy.Extract(63, 32, symbol_ans))
                list_a.append(symbol_a)
                list_b.append(symbol_b)
                file.write("{0},{1},".format(symbol_a, symbol_b))

            for ans in solution1:
                symbol_ans = claripy.BVV(ans, 64)
                symbol_c = solution_state.solver.eval(claripy.Extract(31, 0, symbol_ans))
                symbol_d = solution_state.solver.eval(claripy.Extract(63, 32, symbol_ans))
                list_c.append(symbol_c)
                list_d.append(symbol_d)
                file.write("{0},{1}\n=".format(symbol_c, symbol_d))
            print("symbol_a: ", list_a)
            print("symbol_b: ", list_b)
            print("symbol_a: ", list_c)
            print("symbol_b: ", list_d)
    else:
        raise Exception('Could not find the solution')
    file.close()
if __name__ == "__main__":
    Go()    